import {Coin, Payment} from './model';

export const INSERT_PAYMENT = 'insert-payment';
export const INSERT_EVENT = 'insert-event';
export const FETCH_PAYMENT = 'fetch-payment';

export interface InsertPaymentCmd {
    coin: Coin | string
    address: string
    encryptedPrivateKey: string
    challenge: string
    callbackUrl: string
}

export interface InsertPaymentCmdRes extends Payment {
}

export interface InsertEventCmd {
    coin: Coin | string
    address: string
    type: string
    attemptedAt: string
    successful: Boolean
    error?: string
}

export interface InsertEventCmdRes extends Payment {
}

export interface FetchPaymentQry {
    coin: Coin | string
    address: string
}

export interface FetchPaymentQryRes extends Payment {
}
