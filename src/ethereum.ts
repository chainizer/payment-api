export const CREATE_ACCOUNT = 'create-account';
export const WITHDRAW_ACCOUNT = 'withdraw-account';
export const GET_BALANCE = 'get-balance';

export interface CreateAccountCmd {
}

export interface CreateAccountCmdRes {
    privateKey: string
    address: string
}

export interface WithdrawAccountCmd {
    privateKey: string
    to: string
    message?: string
}

export interface WithdrawAccountCmdRes {
    userTx: string
    feeTx?: string
}

export interface GetBalanceQry {
    address: string
}

export interface GetBalanceQryRes {
    balance: string
}
