import {Coin} from './model';

export const CHECK_PAYMENT = 'check-payment';

export interface CheckPaymentCmd {
    timeToLive: string
    coin: Coin | string
    address: string
    challenge: string
}

export interface CheckPaymentCmdRes {
    success: boolean
    queued: boolean
}
