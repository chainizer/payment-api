export enum Coin {
    ethereum = 'ethereum'
}

export enum PaymentEventType {
    notification = 'notification',
    withdraw = 'withdraw'
}

export interface PaymentEvent {
    type: PaymentEventType
    attemptedAt: string
    successful: Boolean
    error?: string
}

export interface Payment {
    coin: Coin | string
    address: string
    encryptedPrivateKey: string
    challenge: string
    callbackUrl: string
    events?: PaymentEvent[]
}
