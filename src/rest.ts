import {Coin, Payment, PaymentEvent} from './model';

export const CREATE_PAYMENT = 'create-payment';
export const WITHDRAW_PAYMENT = 'withdraw-payment';
export const GET_PAYMENT = 'get-payment';

export interface CreatePaymentCmd {
    coin: Coin | string
    callbackUrl: string
}

export interface CreatePaymentCmdRes {
    token: string
    address: string
}

export interface WithdrawPaymentCmd {
    coin: Coin | string
    address: string
    to: string
    message?: string
}

export interface WithdrawPaymentCmdRes {
    userTx: string
    feeTx?: string
}

export interface GetPaymentQry {
    coin: Coin | string
    address: string
}

export interface GetPaymentQryRes {
    coin: Coin | string
    address: string
    callbackUrl: string
    balance: string
    events?: PaymentEvent[]
}
