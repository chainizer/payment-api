export class PaymentNotFound extends Error {
    constructor(public name = 'PaymentNotFound', public status = 404) {
        super(`Payment cannot be found.`);
    }
}

export class NotAuthorized extends Error {
    constructor(public name = 'NotAuthorized', public status = 401) {
        super(`You are not authorized to perform this action.`);
    }
}

export class NotEnoughCoins extends Error {
    constructor(
        amount: string,
        unit: string,
        public status = 400,
        public name = 'NotEnoughCoins'
    ) {
        super(`Not enough coins to create a transaction: ${amount} ${unit}`);
    }
}
